const { series, src, dest } = require('gulp');
const sourcemaps = require("gulp-sourcemaps");
const shell = require("gulp-shell");
const ts = require("gulp-typescript");
const _tslint = require("gulp-tslint");
const stylish = require("tslint-stylish");
const replace = require("gulp-replace");
const istanbul = require("gulp-istanbul");
const _mocha = require("gulp-mocha");
const remapIstanbul = require("remap-istanbul/lib/gulpRemapIstanbul");
const args = require("yargs").argv;
const tsProject = ts.createProject('tsconfig.json');
const del = require('del');
const merge = require('merge-stream');

// -------------------------------------------------------------------------
// General tasks
// -------------------------------------------------------------------------

/**
 * Creates a delay and resolves after 15 seconds.
 */
function wait(cb: Function) {
    setTimeout(() => cb(), 15000);
}

/**
 * Creates a short dlay and resolves after 3 seconds.
 */
function waitShort(cb: Function) {
    setTimeout(() => cb(), 3000);
}

/**
 * Cleans dist folder.
 */
function clean(cb: Function) {
    return del(["./dist/**"], cb);
}

/**
 * Runs typescript files compilation.
 */
function compile() {
    // return gulp.src("package.json", { read: false })
    //     .pipe(shell(["npm run compile"]));
    return src('./src/**/*.ts', { base: '.' })
        .pipe(sourcemaps.init()) // get ready for some maps...
        .pipe(tsProject())
        .pipe(sourcemaps.write()) // BAM
        .pipe(dest('.'));
}

// -------------------------------------------------------------------------
// Main Packaging and Publishing tasks
// -------------------------------------------------------------------------

/**
 * Publishes a package to npm from ./build/package directory.
 */
function packagePublish() {
    return src("package.json", { read: false })
        .pipe(shell([
            "cd ./dist/package && npm publish"
        ]));
}

/**
 * Publishes a package to npm from ./build/package directory with @next tag.
 */
function packagePublishNext() {
    return src("package.json", { read: false })
        .pipe(shell([
            "cd ./dist/package && npm publish --tag next"
        ]));
}

/**
 * Publishes a package to npm from ./build/package directory with @latest tag.
 */
function packagePublishLatest() {
    return src("package.json", { read: false })
        .pipe(shell([
            "cd ./dist/package && npm publish --tag latest"
        ]));
}

/**
 * Copies all sources to the package directory.
 */
async function packageCompile() {

    const tsProject = ts.createProject('tsconfig.json');
    const tsResult = src(['src/main/**/*.ts'])
        .pipe(sourcemaps.init())
        .pipe(tsProject());
    return merge(tsResult, tsResult.js)
        .pipe(sourcemaps.write('.'))
        .pipe(dest('./dist/package'));

    
    // const tsProject = ts.createProject("tsconfig.json", { typescript: require("typescript") });


    // return tsProject.src()
    //     .pipe(tsProject()).js            
    //         .pipe(sourcemaps.write(".", { sourceRoot: "", includeContent: true }))
    //         .pipe(dest("dist/package"))
    //     // .pipe(tsProject()).dts
    //     //     .pipe(dest("dist/package"))        

    // const tsResult = src(["./src/main/**/*.ts", "./node_modules/@types/**/*.ts"])
    //     .pipe(sourcemaps.init())
    //     .pipe(tsProject())
    //     .js.pipe(dest("./dist/package"))

    // return tsResult
    // await tsResult.pipe(dest("./dist/package"))
    // await tsResult.dts.pipe(dest("./dist/package"))
    // await tsResult.js
    //         .pipe(sourcemaps.write(".", { sourceRoot: "", includeContent: true }))
    //         .pipe(dest("./dist/package"))
    // await Promise.all([
    //     ),
    //     ),
    //     )
    // ]);

    return;
}

/**
 * Moves all compiled files to the final package directory.
 */
function packageMoveCompiledFiles() {
    return src("./dist/package/src/**/*")
        .pipe(dest("./dist/package"));
}

/**
 * Removes /// <reference from compiled sources.
 */
function packageReplaceReferences() {
    return src("./dist/package/**/*.d.ts")
        .pipe(replace(`/// <reference types="node" />`, ""))
        .pipe(dest("./dist/package"));
}

/**
 * Moves all compiled files to the final package directory.
 */
function packageClearPackageDirectory(cb: Function) {
    return del([
        "dist/package/src/**"
    ], cb);
}

/**
 * Change the "private" state of the packaged package.json file to public.
 */
function packagePreparePackageFile() {
    return src("./package.json")
        .pipe(replace("\"private\": true,", "\"private\": false,"))
        .pipe(dest("./dist/package"));
}

/**
 * Copies README.md into the package.
 */
function packageCopyReadme() {
    return src("./README.md")
        .pipe(replace(/```typescript([\s\S]*?)```/g, "```javascript$1```"))
        .pipe(dest("./dist/package"));
}


// function package() {
//     return [
//         "clean",
//         ["packageCompile"],
//         "packageMoveCompiledFiles",
//         [
//             "packageClearPackageDirectory",
//             "packageReplaceReferences",
//             "packagePreparePackageFile",
//             "packageCopyReadme",
//             // "packageCopyShims"
//         ],
//     ];
// }




// -------------------------------------------------------------------------
// Run tests tasks
// -------------------------------------------------------------------------

/**
 * Runs ts linting to validate source code.
 */
function tslint() {
    return src(["./src/main/**/*.ts", "./src/test/**/*.ts"])
        .pipe(_tslint())
        .pipe(_tslint.report(stylish, {
            emitError: true,
            sort: true,
            bell: true
        }));
}

/**
 * Runs before test coverage, required step to perform a test coverage.
 */
function coveragePre() {
    return src(["./dist/main/ts/**/*.js"])
        .pipe(istanbul())
        .pipe(istanbul.hookRequire());
}

/**
 * Runs post coverage operations.
 */
function coveragePost() {
    return src(["./dist/main/ts/**/*.js"])
        .pipe(istanbul.writeReports({
            reporters: ['json']
        }));
}

/**
 * Runs mocha tests.
 */
function runTests() {
    return src(["./dist/test/ts/**/*.js"])
        .pipe(_mocha({
            bail: true,
            grep: !!args.grep ? new RegExp(args.grep) : undefined,
            timeout: 15000
        }));
}

/**
 * Remaps after coverage.
 */
function coverageRemap() {
    return src('./coverage/coverage-final.json')
        .pipe(remapIstanbul({
            reports: {
                'html': './coverage',
                'text-summary': null,
                'text': null,
                'lcovonly': './coverage/lcov.info'
            }
        }))
        // Enforce a coverage of at least 70%
        .pipe(istanbul.enforceThresholds({ thresholds: { global: 10 } }));
}


/**
 * Creates a package that can be published to npm.
 */
const package = series(
    clean,
    packageCompile, 
    packageMoveCompiledFiles,
    packageClearPackageDirectory,
    packageReplaceReferences,
    packagePreparePackageFile,
    packageCopyReadme
);

// -------------------------------------------------------------------------
// Compose Tasks
// -------------------------------------------------------------------------

/**
 * Compiles the code and runs tests + makes coverage report.
 */
const tests = series(
    clean,
    compile,
    coveragePre,
    runTests,
    coveragePost,
    coverageRemap
);

/**
* Runs tests, but creates a small delay before running them to make sure to give time for docker containers to be initialized.
*/
const ciTests = series(
    clean,
    compile,
    tslint,
    wait,
    coveragePre,
    runTests,
    coveragePost,
    coverageRemap
);

/**
 * Creates a package and publishes it to npm.
 */
const publish = series(tests, package, waitShort, packagePublishLatest);

/**
 * Creates a package and publishes it to npm with @next tag.
 */
const publishNext = series(tests, package, packagePublishNext);

exports.publishNext = publishNext;
exports.publish = publish;
exports.tests = tests;
exports.ciTests = ciTests;
exports.package = package;


exports.wait = wait;
exports.waitShort = waitShort;
exports.clean = clean;
exports.compile = compile;
exports.packagePublish = packagePublish;
exports.packagePublishNext = packagePublishNext;
exports.packagePublishLatest = packagePublishLatest;
exports.packageCompile = packageCompile;
exports.packageMoveCompiledFiles = packageMoveCompiledFiles;
exports.packageReplaceReferences = packageReplaceReferences;
exports.packageClearPackageDirectory = packageClearPackageDirectory;
exports.packagePreparePackageFile = packagePreparePackageFile;
exports.packageCopyReadme = packageCopyReadme;