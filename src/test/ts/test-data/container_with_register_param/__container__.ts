import SomeClass from "../SomeClass";
import Container from "../../../../main/ts/Container";

const registerBeans = (container: Container) => {
    const someClass = new SomeClass();
    container.setBean('someClass', someClass);
};

const registerClazzes = (container: Container) => {
    container.setClazz('SomeClass', SomeClass);
};

const registerParams = ( container: Container) => {
    container.setParams('someKey', 'someValue');
};


export {
    registerBeans,
    registerClazzes,
    registerParams
};