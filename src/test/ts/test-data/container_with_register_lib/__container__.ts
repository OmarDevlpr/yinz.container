import SomeClass from "../SomeClass";
import Container from "../../../../main/ts/Container";
import * as assert from "assert";

const registerBeans = (container: Container) => {
    const someClass = new SomeClass();
    container.setBean('someClass', someClass);
};

const registerClazzes = (container: Container) => {
    container.setClazz('SomeClass', SomeClass);
};

const registerLibs = ( container: Container) => {
    container.setLib('assert', assert);
};


export {
    registerBeans,
    registerClazzes,
    registerLibs
};