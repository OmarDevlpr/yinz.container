import * as assert from "assert";
import Container from "../../main/ts/Container";





describe('| commons.Container', () => {
    describe('| #constructor', () => {


        it('\n' +
            '            | should:\n' +
            '            | -> instantiate an new Container \n' +
            '            | given that:\n' +
            '            | * options provided is empty \n' +
            '', () => {

                try {
                    const container = new Container({ debug: true});

                    assert.ok(container);
                } catch (e) {
                    assert.ok(false, 'exception: ' + (e.stack || e.message));
                }

        });

        it('\n' +
            '            | should:\n' +
            '            | -> instantiate an new Container \n' +
            '            | given that:\n' +
            '            | * options modules is empty \n' +
            '', () => {

                try {
                    const container = new Container({ debug: true, modules: [] });

                    assert.ok(container);
                } catch (e) {
                    assert.ok(false, 'exception: ' + (e.stack || e.message));
                }

            });


        it('\n' +
            '            | should:\n' +
            '            | -> register beans and clazzes of the underlying module \n' +
            '            | given that:\n' +
            '            | * options contains modules with path to __container__ file \n' +
            '            | * __container__ file is registering the clazz and bean \n' +
            '', () => {

                try {

                    // 1. prepare test-data
                    const options = {
                        modules: [
                            process.cwd() + '/src/test/ts/test-data'
                        ],
                        debug: true
                    };

                    // 2. execute test
                    const container = new Container(options);

                    // 3. check outcome
                    assert.ok(container);
                    assert.ok(container.getBean('someClass'));
                    assert.ok(container.getClazz('SomeClass'));
                    assert.strictEqual(Object.keys(container.getClazzes()).length, 1);
                    assert.strictEqual(Object.keys(container.getBeans()).length, 1);                    


                } catch (e) {
                    assert.ok(false, 'exception: ' + (e.stack || e.message));
                }

            });

        it('\n' +
            '            | should:\n' +
            '            | -> register beans and clazzes of the underlying module \n' +
            '            | -> register params provided in params.yml \n' +
            '            | -> register  \n' +
            '            | given that:\n' +
            '            | * options contains modules with path to __container__ file \n' +
            '            | * __container__ file is registering the clazz and bean \n' +
            '            | * params file is provided \n' +
            '', () => {

                try {

                    // 1. prepare test-data
                    const options = {
                        modules: [
                            process.cwd() + '/src/test/ts/test-data'
                        ],
                        paramsFile: process.cwd() + '/src/test/ts/test-data/params.yml',
                        debug: true
                    };

                    // 2. execute test
                    const container = new Container(options);

                    // 3. check outcome
                    assert.ok(container);
                    assert.ok(container.getBean('someClass'));
                    assert.ok(container.getClazz('SomeClass'));
                    assert.ok(container.getParam('someKey'));
                    assert.strictEqual(container.getParam('someKey'), 'someValue');
                    assert.strictEqual(Object.keys(container.getClazzes()).length, 1);
                    assert.strictEqual(Object.keys(container.getBeans()).length, 1);
                    assert.strictEqual(Object.keys(container.getParams()).length, 1);


                } catch (e) {
                    assert.ok(false, 'exception: ' + (e.stack || e.message));
                }

            });

        it('\n' +
            '            | should:\n' +
            '            | -> register beans and clazzes of the underlying module \n' +
            '            | -> register params provided in container file \n' +            
            '            | given that:\n' +
            '            | * options contains modules with path to __container__ file \n' +
            '            | * __container__ file is registering the clazz and bean \n' +
            '            | * params file is not provided \n' +
            '', () => {

                try {

                    // 1. prepare test-data
                    const options = {
                        modules: [
                            process.cwd() + '/src/test/ts/test-data/container_with_register_param'
                        ],                        
                        debug: true
                    };

                    // 2. execute test
                    const container = new Container(options);

                    // 3. check outcome
                    assert.ok(container);
                    assert.ok(container.getBean('someClass'));
                    assert.ok(container.getClazz('SomeClass'));
                    assert.ok(container.getParam('someKey'));
                    assert.strictEqual(container.getParam('someKey'), 'someValue');
                    assert.strictEqual(Object.keys(container.getClazzes()).length, 1);
                    assert.strictEqual(Object.keys(container.getBeans()).length, 1);
                    assert.strictEqual(Object.keys(container.getParams()).length, 1);


                } catch (e) {
                    assert.ok(false, 'exception: ' + (e.stack || e.message));
                }

            });


        it('\n' +
            '            | should:\n' +
            '            | -> register beans and clazzes of the underlying module \n' +
            '            | -> register libs provided in container file \n' +
            '            | given that:\n' +
            '            | * options contains modules with path to __container__ file \n' +
            '            | * __container__ file is registering the clazz and bean \n' +
            '            | * params file is not provided \n' +
            '', () => {

                try {

                    // 1. prepare test-data
                    const options = {
                        modules: [
                            process.cwd() + '/src/test/ts/test-data/container_with_register_lib'
                        ],
                        debug: true
                    };

                    // 2. execute test
                    const container = new Container(options);

                    // 3. check outcome
                    assert.ok(container);
                    assert.ok(container.getBean('someClass'));
                    assert.ok(container.getClazz('SomeClass'));
                    assert.ok(container.getLib('assert'));                    
                    assert.strictEqual(Object.keys(container.getClazzes()).length, 1);
                    assert.strictEqual(Object.keys(container.getBeans()).length, 1);
                    assert.strictEqual(Object.keys(container.getLibs()).length, 1);


                } catch (e) {
                    assert.ok(false, 'exception: ' + (e.stack || e.message));
                }

            });


        it('\n' +
            '            | should:\n' +
            '            | -> register beans and clazzes of the underlying module \n' +
            '            | -> register params provided in params.yml \n' +
            '            | -> override the values in params.yml \n' +
            '            | -> env is production \n' +
            '            | given that:\n' +
            '            | * options contains modules with path to __container__ file \n' +
            '            | * __container__ file is registering the clazz and bean \n' +
            '            | * params file is provided \n' +
            '', () => {

                try {

                    // 1. prepare test-data
                    const options = {
                        modules: [
                            process.cwd() + '/src/test/ts/test-data/'
                        ],
                        paramsFile: process.cwd() + '/src/test/ts/test-data/params.yml',
                        debug: true
                    };

                    process.env.NODE_ENV = "production";

                    // 2. execute test
                    const container = new Container(options);

                    // 3. check outcome
                    assert.ok(container);
                    assert.ok(container.getBean('someClass'));
                    assert.ok(container.getClazz('SomeClass'));                    
                    assert.strictEqual(Object.keys(container.getClazzes()).length, 1);
                    assert.strictEqual(Object.keys(container.getBeans()).length, 1);
                    

                    assert.strictEqual(container.getParam('someKey'), 'SOME_VALUE_FROM_ENV');


                    // 4. cleanup env
                    process.env.NODE_ENV = "";


                } catch (e) {
                    assert.ok(false, 'exception: ' + (e.stack || e.message));
                }

            });


        it('\n' +
            '            | should:\n' +
            '            | -> register beans and clazzes of the underlying module \n' +
            '            | -> register params provided in params.yml \n' +
            '            | -> override the values in params.yml \n' +
            '            | -> env is production \n' +
            '            | given that:\n' +
            '            | * options contains modules with path to __container__ file \n' +
            '            | * __container__ file is registering the clazz and bean \n' +
            '            | * params file is provided \n' +
            '            | * params file has nested value \n' +
            '', () => {

                try {

                    // 1. prepare test-data
                    const options = {
                        modules: [
                            process.cwd() + '/src/test/ts/test-data/'
                        ],
                        paramsFile: process.cwd() + '/src/test/ts/test-data/nested_params_.yml',
                        debug: true
                    };

                    process.env.NODE_ENV = "production";

                    // 2. execute test
                    const container = new Container(options);

                    // 3. check outcome
                    assert.ok(container);
                    assert.ok(container.getBean('someClass'));
                    assert.ok(container.getClazz('SomeClass'));
                    assert.strictEqual(Object.keys(container.getClazzes()).length, 1);
                    assert.strictEqual(Object.keys(container.getBeans()).length, 1);


                    assert.strictEqual(container.getParam('someKey')['someOtherKeyOne'], 'SOME_VALUE_ONE_FROM_ENV');
                    assert.strictEqual(container.getParam('someKey')['someOtherKeyTwo'], 'SOME_VALUE_TWO_FROM_ENV');


                    // 4. cleanup env
                    process.env.NODE_ENV = "";


                } catch (e) {
                    assert.ok(false, 'exception: ' + (e.stack || e.message));
                }

            });


    });

});
