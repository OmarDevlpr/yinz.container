import * as yaml from "js-yaml";
import * as fs from "fs";
import * as deepExtend from "deep-extend";
import * as _ from "lodash";
// for env
require('dotenv').config();

export interface ContainerOptions {
    debug?: boolean;
    params?: any;
    modules?: string[];
    paramsFile?: string;
}

export default class Container {


    private container: {
        params: any,
        libs: any,
        clazzes: any,
        beans: any,
    };
    private debug: boolean;

    constructor(options: ContainerOptions) {

        options = options || {};

        this.container = {
            params: options.params || {},
            libs: {},
            clazzes: {},
            beans: {}
        };        

        this.debug = !!options.debug;

        if (this.debug) {
            console.log('Assembler params from options...');
        }
        if (this.debug) {
            console.log(JSON.stringify(this.container.params));
        }



        if (options.paramsFile) {
            // parameters within 'options' object overrides parameters read from 'paramsFile'.
            let paramsFromFile = yaml.safeLoad(fs.readFileSync(options.paramsFile, 'utf8'));

            if (this.debug) {
                console.log('Assembler params from file...');
            }
            if (this.debug) {
                console.log(JSON.stringify(paramsFromFile));
            }

            this.container.params = deepExtend(paramsFromFile, this.container.params);

            if (this.debug) {
                console.log('Assembler final params...');
            }
            if (this.debug) {
                console.log(JSON.stringify(this.container.params));
            }
        }

       

        if (options.modules) {
            this.assemble(options.modules);
        }

        // TODO        
        if (process.env.NODE_ENV === "production" || process.env.NODE_ENV === "production") {
            this.setEnvironmentVariables();
        }

    }

    private setEnvironmentVariables() {

        // Environment variables should be loaded after the file/params options are done loading
        // we will then conscruct environement variable keys based on options we currently have
        // Ex: db: {host: 'somehome'} should resolve to -> DB_HOST

        // let envVariables = process.env;
        for ( let key in this.container.params ) {

            const val = this.container.params[key];
            const envVars = [];
            this.constructEnvKey(key, val, envVars);            
            for ( let variable of envVars) {
                if (process.env[variable]) {
                    this.setTheEnvKey(variable, this.container.params[key], key);
                    
                }
            }            
        }
    }

    private setTheEnvKey(envKey: string, paramObj: object, paramKey: string) {
        
        const envKeyBits = envKey.split('_');

        if ( !_.isObject(paramObj)) {
            this.container.params[paramKey] = process.env[envKey];
        }

        for ( let bit of envKeyBits ) {
  
            const paramObjKeys = Object.keys(paramObj);

            for ( let currentParamObjKey of paramObjKeys ) {                
                
                if (bit === _.toUpper(currentParamObjKey) && !_.isObject(paramObj[currentParamObjKey])) {
                    paramObj[currentParamObjKey] = process.env[envKey];
                }
            }

        }        

    }

    private constructEnvKey(paramKey: string, paramVal: any, envKeys: string[], currentEnvKey?: string) {

        currentEnvKey = currentEnvKey || '';
        currentEnvKey = currentEnvKey === '' ? currentEnvKey : currentEnvKey + '_';
        const upperParamKey = _.toUpper(paramKey);

        currentEnvKey = currentEnvKey + upperParamKey;

        if (_.isObject(paramVal)) {
            for (let key in paramVal) {
                const val = paramVal[key];
                this.constructEnvKey(key, val, envKeys, currentEnvKey);
            }
        } else {
            envKeys.push(currentEnvKey);
        }

    }

    public assemble(modules: string[] ) {

        if (modules.length < 1) {
            if (this.debug) { console.warn('No module to assemble!'); }
            return;
        }
        

        // let err, name, spec, path, type;

        let __self__ = this;

        if (this.debug) { console.log('About to assemble modules ' + JSON.stringify(modules) + '...'); }

        for (let moduleIndex = 0; moduleIndex < modules.length; moduleIndex++) {


            console.log('MODULE :: >>', modules[moduleIndex]);

            let module = modules[moduleIndex];
            if (this.debug) { console.log('  About to assemble module "' + module + '"...'); }

            if (this.debug) { console.log('    ...loading module "' + module + '"...'); }
            let container = require(module + '/__container__');


            if (typeof container.registerParams === 'function') {
                if (this.debug) { console.log('    ...loading params used by module "' + module + '"...'); }
                container.registerParams(__self__);
            }

            if (typeof container.registerLibs === 'function') {
                if (this.debug) { console.log('    ...loading libs used by module "' + module + '"...'); }
                container.registerLibs(__self__);
            }

            if (typeof container.registerClazzes === 'function') {
                if (this.debug) { console.log('    ...loading clazzes of module "' + module + '"...'); }
                container.registerClazzes(__self__);
            }

            if (typeof container.registerBeans === 'function') {
                if (this.debug) { console.log('    ...loading beans of module "' + module + '"...'); }
                container.registerBeans(__self__);
            }


        }

        if (this.debug) { console.log('...modules ' + JSON.stringify(modules) + ' assembled with success.'); }

    }


    setBean(name: string, bean) {
        this.container.beans[name] = bean;

    }

    getBean<T>(name: string): T {
        return this.container.beans[name];
    }

    getBeans() {
        return this.container.beans;
    }

    setClazz(name: string, clazz) {
        this.container.clazzes[name] = clazz;
    }

    getClazz(name: string) {
        return this.container.clazzes[name];
    }

    getClazzes() {
        return this.container.clazzes;
    }

    setParams(name: string, param) {
        this.container.params[name] = param;
    }

    getParams() {
        return this.container.params;
    }

    getParam(name: string) {
        return this.container.params[name];
    }

    setLib(name: string, lib) {
        this.container.libs[name] = lib;
    }

    getLibs() {
        return this.container.libs;
    }

    getLib(name: string) {
        return this.container.libs[name];
    }

}